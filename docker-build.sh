#!/bin/bash

__START_TIME=$(date +%s)

appName=$1
appVersion=$2
image="$appName:$appVersion"

echo "Install dependencies..."
npm i
__INSTALL_BUILD_DEPENDENCIES_TIME=$(date +%s)

echo "Build bundle..."
npm run build:js
__BUILD_BUNDLE_TIME=$(date +%s)

echo "Remove build dependencies..."
npm prune --production
__REMOVE_BUILD_DEPENDENCIES_TIME=$(date +%s)

echo "Build container..."
docker build -f ./Dockerfile -t $image .
__BUILD_CONTAINER_TIME=$(date +%s)

__END_TIME=$(date +%s)
echo "Build container time: $(( $__BUILD_CONTAINER_TIME - $__REMOVE_BUILD_DEPENDENCIES_TIME ))s"
echo "Total time: $(( $__END_TIME - $__START_TIME ))s"