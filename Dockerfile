FROM mhart/alpine-node
LABEL maintainer="Nikita Shuklin"

WORKDIR /build
ADD ./ /

RUN npm i -g serve

CMD serve -l 4000
