import React from 'react';
import '../../styles/Components/Layout/index.css'

type Props = {
    children: React.ReactNode | [React.ReactNode]
}

export default function Layout(props: Props) {
    return (
        <div className='layout'>
            {props.children}
        </div>
    )
}