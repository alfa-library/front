import * as React from 'react';
import * as classnames from 'classnames';

import '../../styles/Components/Grid-item/index.css';

type Props = {
    cols: number;
    hideSmall?: boolean;
    colsSmall?: number;
    hideMedium?: boolean;
    colsMedium?: number;
    className?: string;
    children?: JSX.Element[] | JSX.Element | Element[] | Element;
    testId?: string;
};

export class GridItem extends React.Component<Props> {
    render() {
        return (
            <div
                className={
                    (classnames as any)('grid-item', {
                        [`grid-item_large_col-${this.props.cols}`]: this.props.cols,
                        [`grid-item_small_col-${this.props.colsSmall}`]: this.props.colsSmall,
                        [`grid-item_medium_col-${this.props.colsMedium}`]: this.props.colsMedium,
                        'grid-item_hideSmall': this.props.hideSmall,
                        'grid-item_hideMedium': this.props.hideMedium,
                    })
                }
                data-test-id={ this.props.testId }
            >
                {this.props.children}
            </div>
        );
    }
}

export default GridItem;
