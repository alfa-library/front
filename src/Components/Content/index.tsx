import React from 'react';
import '../../styles/Components/Content/index.css'

type Props = {
    children: React.ReactNode | [React.ReactNode]
}

export default function Content(props: Props) {
    return (
        <div className='content'>
            <div className='content__body'>
                {props.children}
            </div>
        </div>
    )
}