import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Spin from 'arui-feather/spin';
import UserName from '../UserName';
import {
    logoutSagaAC
} from '../../Redux/Ducks/auth'

import { UserType } from '../../types/user';
import { ProcessStateType } from '../../utils';

import '../../styles/Components/UserBar/index.css';


type PropsType = {
    user: UserType,
    processData: ProcessStateType
}

export default function UserBar(props: PropsType) {
    const dispatch = useDispatch();

    function handleLogoutClick(e: React.MouseEvent) {
        e.preventDefault();
        dispatch(logoutSagaAC());
    }
    function renderBar() {
        return (
            <div>
                {
                    props.user._id ?
                        <div>
                            <UserName user={ props.user }/>&nbsp;|&nbsp;
                            <Link
                                to={ '/' }
                                onClick={ handleLogoutClick }
                            >Выход</Link>
                        </div> :
                        <div>
                            <Link
                                to={ '/login' }
                            >Вход</Link>
                            &nbsp;|&nbsp;
                            <Link
                                to={ '/signup' }
                            >Регистрация</Link>
                        </div>

                }
            </div>
        )
    }

    return (
        <div className='user-bar'>
            {
                props.processData.isLoading ?
                    <Spin
                        size={ 'm' }
                        visible={ true }
                    /> :
                    renderBar()
            }

        </div>
    );
}