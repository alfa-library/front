import React from 'react';
import Logo from '../../static/img/logo.svg'
import '../../styles/Components/Sidebar/index.css';

export default function Sidebar() {
    return (
        <div className='sidebar'>
            <div className='sidebar__logo'>
                <img src={ Logo } alt=""/>
            </div>
        </div>
    )
}


