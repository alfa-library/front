import * as React from 'react';
import * as classnames from 'classnames';

import '../../styles/Components/Grid/index.css';

type Props = {
    wide?: boolean
    className?: string
    wrap?: boolean
    withoutSpaces?: boolean;
    bottomMargin?: boolean;
    children: JSX.Element[] | JSX.Element | Element[] | Element;
    testId?: string;
};

export class Grid extends React.Component<Props> {
    static defaultProps = {
        wide: true
    };

    render() {
        return (
            <div
                className={
                    (classnames as any)('grid', {
                            'grid_wide': this.props.wide,
                            'grid_wrap': this.props.wrap,
                            'grid_withoutSpaces': this.props.withoutSpaces,
                            'grid_bottomMargin': this.props.bottomMargin
                        })
                }
                data-test-id={ this.props.testId }
            >
                {this.props.children}
            </div>
        );
    }
}

export default Grid;
