import React from 'react';
import { UserType } from '../../types/user';

type Props = {
    user: UserType
}

export default function (props: Props) {
    return (
        <span>
            { props.user.lastName } { props.user.firstName }
        </span>
    )
}