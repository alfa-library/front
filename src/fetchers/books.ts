import { createUrl } from './basic-fetch';
import basicFetch from './basic-fetch';
import { AddBookType } from '../types/books';

export function fetchBooksFetcher() {
    return basicFetch('GET', createUrl('/book/all'), {})
}

export function addBookFetcher(book: AddBookType) {
    return basicFetch('POST', createUrl('/book/add'), book);
}

export function takeBookFetcher(bookId: String) {
    return basicFetch('POST', createUrl(`/book/${bookId}/take`), {});
}

export function returnBookFetcher(bookId: String) {
    return basicFetch('POST', createUrl(`/book/${bookId}/return`), {});
}