import basicFetch, { createUrl } from './basic-fetch';

export function fetchUserFetcher() {
    return basicFetch('GET', createUrl('/user'), {})
}