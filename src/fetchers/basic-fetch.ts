import axios from 'axios';
import { MethodsType } from '../types/fetchers';

export default function basicFetch(method: MethodsType, url:string, data: any) {
    return axios({
        method, url, data, withCredentials: true
    })
}

export function createUrl(path:string) {
    return `http://${process.env.NODE_ENV === 'production' ? '165.22.82.104' : 'localhost'}:${process.env.PORT || 8080}${path}`
}