import basicFetch, { createUrl } from './basic-fetch';
import { LoginDataType, SignUpDataType } from '../types/user';

export function signUpPostFetcher(data: SignUpDataType) {
    return basicFetch('POST', createUrl('/signup'), data)
}

export function loginPostFetcher(data: LoginDataType) {
    return basicFetch('POST', createUrl('/login'), data)
}

export function logoutPostFetcher() {
    return basicFetch('POST', createUrl('/logout'), {})
}