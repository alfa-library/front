export type UserType = {
    _id: string;
    firstName: string;
    lastName: string;
    email: string;
}

export const defaultUser: UserType = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
};

export type SignUpDataType = UserType & {
    password: string;
}

export type LoginDataType = {
    email: string;
    password: string;
}