import { UserType } from './user';

export type BookType = {
    _id: string,
    title: string,
    authors?: string,
    owner: UserType,
    reader?: UserType,
    startReadDate?: Date
}

export type AddBookType = {
    title: string,
    authors: string,
    ownerEmail: string,
}