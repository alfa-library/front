import yup from 'yup';

// Validation

type ValueTypes = {
    [K: string]: string | number
}

export async function validateForm(values:ValueTypes, schema: yup.Schema<{}>) {
    try {
        await schema.validate(values, { abortEarly: false })
    } catch (err) {
        return err.inner.reduce((formError: any, innerError: any) => ({
            ...formError,
            [innerError.path]: innerError.message
        }), {});
    }
}

// for Redux state

export type ProcessStateType = {
    isLoading: boolean;
    isError: boolean;
    isSuccess: boolean
}

export const initialProcessState: ProcessStateType = {
    isLoading: false,
    isError: false,
    isSuccess: false,
};

export function returnStartState(): ProcessStateType {
    return {
        isLoading: true,
        isError: false,
        isSuccess: false,
    }
}

export function returnSuccessState(): ProcessStateType {
    return {
        isLoading: false,
        isError: false,
        isSuccess: true,
    }
}

export function returnFailState(): ProcessStateType {
    return {
        isLoading: false,
        isError: true,
        isSuccess: false
    }
}