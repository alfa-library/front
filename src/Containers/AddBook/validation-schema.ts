import * as yup from 'yup';

const REQUIRED_ERROR = 'Обязательное поле';

const schemas = yup.object().shape({
    title: yup
        .string()
        .required(REQUIRED_ERROR),
    authors: yup
        .string()
        .required(REQUIRED_ERROR),
    ownerEmail: yup
        .string()
        .required(REQUIRED_ERROR)
});

export default schemas;