import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Field, FormRenderProps } from 'react-final-form'
import Heading from 'arui-feather/heading'
import Input from 'arui-feather/input';
import Button from 'arui-feather/button';
import Grid from '../../Components/Grid';
import GridItem from '../../Components/Grid-item';

import { addBookSagaAC } from '../../Redux/Ducks/books';
import { getUser } from '../../Redux/Ducks/user';
import { AddBookType } from '../../types/books';
import { validateForm } from '../../utils';

import validationSchema from './validation-schema';

export default function AddBook() {
    const dispatch = useDispatch();
    const userState = useSelector(getUser);
    const initValues: AddBookType = {
        title: '',
        authors: '',
        ownerEmail: userState.email ? userState.email : ''
    };
    function handleSubmit (values: AddBookType) {
        dispatch(addBookSagaAC(values))
    }
    function handleValidation (values: AddBookType) {
        return validateForm(values, validationSchema)
    }
    function renderForm({ handleSubmit, pristine, invalid }: FormRenderProps<AddBookType>) {
        return (
            <form onSubmit={ handleSubmit }>
                <Grid
                    bottomMargin={ true }
                >
                    <GridItem
                        cols={ 3 }
                    >
                        <Field
                            name='title'
                            render={
                                ({ input, meta }) => (
                                    <Input
                                        { ...input }
                                        size='m'
                                        placeholder='На обложке написано'
                                        type='text'
                                        label='Название книги'
                                        width='available'
                                        error={ meta.error }
                                    />
                                )
                            }
                        />
                    </GridItem>
                </Grid>
                <Grid
                    bottomMargin={ true }
                >
                    <GridItem
                        cols={ 3 }
                    >
                        <Field
                            name='authors'
                            render={
                                ({ input, meta }) => (
                                    <Input
                                        { ...input }
                                        size='m'
                                        placeholder='Авторов одной строкой. Через запятую'
                                        type='text'
                                        label='Авторы'
                                        width='available'
                                        error={ meta.error }
                                    />
                                )
                            }
                        />
                    </GridItem>
                </Grid>
                <Grid
                    bottomMargin={ true }
                >
                    <GridItem
                        cols={ 3 }
                    >
                        <Field
                            name='ownerEmail'
                            render={
                                ({ input, meta }) => (
                                    <Input
                                        { ...input }
                                        size='m'
                                        placeholder='Email владельца'
                                        type='text'
                                        label='Авторы'
                                        width='available'
                                        error={ meta.error }
                                    />
                                )
                            }
                        />
                    </GridItem>
                </Grid>
                <Button
                    type="submit"
                    disabled={ invalid }>
                    Submit
                </Button>
            </form>
        )
    }
    return (
        <div>
            <Heading>
                Добавление книги
            </Heading>
            <Form
                onSubmit = { handleSubmit }
                validate = { handleValidation }
                initialValues = { initValues }
                render={ renderForm }
            />
        </div>
    )
}