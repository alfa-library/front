import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Form, Field } from 'react-final-form'
import Input from 'arui-feather/input';
import Button from 'arui-feather/button';
import Grid from '../../Components/Grid';
import GridItem from '../../Components/Grid-item';

import { validateForm } from '../../utils';
import validationSchema from './validation-schema';

import { getAuthState, signUpSagaAC } from '../../Redux/Ducks/auth';
import { SignUpDataType } from '../../types/user';


export default function SignUp() {
    const dispatch = useDispatch();
    const signUpState = useSelector(getAuthState);
    return (
        <div>
            <h1>Регистрация</h1>
            <Form
                onSubmit={ function (values: SignUpDataType) {
                    dispatch(signUpSagaAC(values))
                } }
                validate={ (values) => {
                    return validateForm(values, validationSchema)
                } }
                render={ ({ handleSubmit, pristine, invalid }) => (
                    <form onSubmit={ handleSubmit }>
                        <Grid
                            bottomMargin={ true }
                        >
                            <GridItem
                                cols={ 3 }
                            >
                                <div>{ invalid }</div>
                                <Field
                                    name='lastName'
                                    render={
                                        ({ input, meta }) => (
                                            <Input
                                                { ...input }
                                                size='m'
                                                placeholder='Иванов'
                                                type='text'
                                                label='Фамилия'
                                                width='available'
                                                error={ meta.error }
                                                disabled={ signUpState.isLoading }
                                            />
                                        )
                                    }
                                />
                            </GridItem>
                        </Grid>
                        <Grid
                            bottomMargin={ true }
                        >
                            <GridItem
                                cols={ 3 }
                            >
                                <Field
                                    name='firstName'
                                    render={
                                        ({ input, meta }) => (
                                            <Input
                                                { ...input }
                                                size='m'
                                                placeholder='Иван'
                                                type='text'
                                                label='Имя'
                                                width='available'
                                                error={ meta.error }
                                                disabled={ signUpState.isLoading }
                                            />
                                        )
                                    }
                                />
                            </GridItem>
                        </Grid>
                        <Grid
                            bottomMargin={ true }
                        >
                            <GridItem
                                cols={ 3 }
                            >
                                <Field
                                    name='email'
                                    render={
                                        ({ input, meta }) => (
                                            <Input
                                                { ...input }
                                                size='m'
                                                placeholder='test@test.com'
                                                type='email'
                                                label='Email'
                                                width='available'
                                                error={ meta.error }
                                                disabled={ signUpState.isLoading }
                                            />
                                        )
                                    }
                                />
                            </GridItem>
                        </Grid>
                        <Grid
                            bottomMargin={ true }
                        >
                            <GridItem
                                cols={ 3 }
                            >
                                <Field
                                    name='password'
                                    render={
                                        ({ input, meta }) => (
                                            <Input
                                                { ...input }
                                                size='m'
                                                type='password'
                                                label='Пароль'
                                                width='available'
                                                error={ meta.error }
                                                disabled={ signUpState.isLoading }
                                            />
                                        )
                                    }
                                />
                            </GridItem>
                        </Grid>
                        <Button
                            type="submit"
                            disabled={ invalid || signUpState.isLoading }>
                            Submit
                        </Button>
                    </form>
                ) }
            />
        </div>
    )
}