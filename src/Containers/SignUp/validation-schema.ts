import * as yup from 'yup';

const REQUIRED_ERROR = 'Обязательное поле';

const schemas = yup.object().shape({
    firstName: yup
        .string()
        .required(REQUIRED_ERROR),
    lastName: yup
        .string()
        .required(REQUIRED_ERROR),
    email: yup
        .string()
        .required(REQUIRED_ERROR),
    password: yup
        .string()
        .required(REQUIRED_ERROR)
});

export default schemas;