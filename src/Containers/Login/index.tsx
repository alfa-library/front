import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Form, Field, FormRenderProps } from 'react-final-form'
import Input from 'arui-feather/input';
import Button from 'arui-feather/button';
import Heading from 'arui-feather/heading';
import Grid from '../../Components/Grid';
import GridItem from '../../Components/Grid-item';

import { validateForm } from '../../utils';
import validationSchema from './validation-schema';

import { getAuthState, loginSagaAC } from '../../Redux/Ducks/auth';
import { SignUpDataType } from '../../types/user';


export default function Login() {
    const dispatch = useDispatch();
    const signUpState = useSelector(getAuthState);
    function handleSubmit (values: SignUpDataType) {
        dispatch(loginSagaAC(values))
    }
    function handleValidation (values: SignUpDataType) {
        return validateForm(values, validationSchema)
    }
    function renderForm({ handleSubmit, pristine, invalid }: FormRenderProps<SignUpDataType>) {
        return (
            <form onSubmit={ handleSubmit }>
                <Grid
                    bottomMargin={ true }
                >
                    <GridItem
                        cols={ 3 }
                    >
                        <Field
                            name='email'
                            render={
                                ({ input, meta }) => (
                                    <Input
                                        { ...input }
                                        size='m'
                                        placeholder='test@test.com'
                                        type='email'
                                        label='Email'
                                        width='available'
                                        error={ meta.error }
                                        disabled={ signUpState.isLoading }
                                    />
                                )
                            }
                        />
                    </GridItem>
                </Grid>
                <Grid
                    bottomMargin={ true }
                >
                    <GridItem
                        cols={ 3 }
                    >
                        <Field
                            name='password'
                            render={
                                ({ input, meta }) => (
                                    <Input
                                        { ...input }
                                        size='m'
                                        type='password'
                                        label='Пароль'
                                        width='available'
                                        error={ meta.error }
                                        disabled={ signUpState.isLoading }
                                    />
                                )
                            }
                        />
                    </GridItem>
                </Grid>
                <Button
                    type="submit"
                    disabled={ invalid || signUpState.isLoading }>
                    Submit
                </Button>
            </form>
        )
    }
    return (
        <div>
            <Heading>Вход</Heading>
            <Form
                onSubmit={ handleSubmit }
                validate={ handleValidation }
                render={ renderForm }
            />
        </div>
    )
}