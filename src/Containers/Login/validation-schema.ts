import * as yup from 'yup';

const REQUIRED_ERROR = 'Обязательное поле';

const schemas = yup.object().shape({
    email: yup
        .string()
        .required(REQUIRED_ERROR),
    password: yup
        .string()
        .required(REQUIRED_ERROR)
});

export default schemas;