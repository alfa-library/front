import React from 'react';
import ThemeProvider from 'arui-feather/theme-provider';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import Root from '../Root';

import store, { history } from '../../Redux/configure-store';

const App: React.FC = function () {
    return (
        <Provider store={ store()() }>
            <ThemeProvider theme='alfa-on-white'>
                <ConnectedRouter history={ history }>
                    <Root/>
                </ConnectedRouter>
            </ThemeProvider>
        </Provider>
    );
};

export default App;
