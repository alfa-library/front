import React, { useEffect } from 'react';
import { Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import Sidebar from '../../Components/Sidebar'
import Layout from '../../Components/Layout';
import Content from '../../Components/Content';
import Home from '../Home';
import SignUp from '../SignUp';
import Login from '../Login';
import AddBook from '../AddBook';
import UserBar from '../../Components/UserBar';

import { fetchUserSagaAC, getUser, getUserProcessing } from '../../Redux/Ducks/user';

export default function Root() {
    const dispatch = useDispatch();
    const userState = useSelector(getUser);
    const userProcessingState = useSelector(getUserProcessing);
    useEffect(function () {
        dispatch(fetchUserSagaAC())
    }, []);
    return (
        <Layout>
            <Sidebar/>
            <Content>
                <UserBar
                    user={ userState }
                    processData={ userProcessingState }
                />
                <Route exact path='/' component={ Home }/>
                <Route exact path='/signup' component={ SignUp }/>
                <Route exact path='/login' component={ Login }/>
                <Route exact path='/addBook' component={ AddBook }/>
            </Content>
        </Layout>

    );
};
