import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import Heading from 'arui-feather/heading';
import Button from 'arui-feather/button';
import { BooksStateType, fetchBooksSagaAC, takeBookSagaAC, getBooks, returnBookSagaAC } from '../../Redux/Ducks/books';
import { getUser } from '../../Redux/Ducks/user';
import { BookType } from '../../types/books';
import { defaultUser, UserType } from '../../types/user';

import UserName from '../../Components/UserName';

import Grid from '../../Components/Grid';
import GridItem from '../../Components/Grid-item';

import '../../styles/Containers/Home/index.css'

export default function Home() {
    const dispatch = useDispatch();
    useEffect(function () {
        dispatch(fetchBooksSagaAC())
    }, []);
    const booksData: BooksStateType = useSelector(getBooks);
    const user: UserType = useSelector(getUser);
    function isBookHaveReader(book: BookType): boolean {
        return !!book.reader
    }
    function isBookReaderEqualCurrentUser(book: BookType, user: UserType): boolean {
        return !!book.reader && book.reader._id === user._id
    }
    function handlerAddBookClick() {
        dispatch(push('/addBook'))
    }
    function handlerTakeBookClick(id: string) {
        return function() {
            dispatch(takeBookSagaAC(id))
        }
    }
    function handlerReturnBookClick(id: string) {
        return function() {
            dispatch(returnBookSagaAC(id))
        }
    }
    return (
        <div>
            <Grid
                bottomMargin={ true }
            >
                <GridItem
                    cols={9}
                >
                    <Heading>
                        Книжная полка
                    </Heading>
                </GridItem>
                <GridItem
                    cols={3}
                >
                    <Button
                        onClick={ handlerAddBookClick }
                    >
                        Добавить книгу
                    </Button>
                </GridItem>
            </Grid>

            <table className='books-table'>
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Авторы</th>
                    <th>Владелец</th>
                    <th>Кто читает</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    booksData.books.map(function (item: BookType) {
                        return (
                            <tr
                                key={ item._id }
                            >
                                <td>{ item.title }</td>
                                <td>{ item.authors }</td>
                                <td><UserName user={ item.owner }/></td>
                                <td><UserName user={ item.reader || defaultUser }/></td>
                                <td>
                                    {
                                        !isBookHaveReader(item) ?
                                            <Button
                                                onClick={ handlerTakeBookClick(item._id) }
                                            >
                                                Взять книгу почитать!
                                            </Button> :
                                            isBookReaderEqualCurrentUser(item, user) ?
                                                <Button
                                                    onClick={ handlerReturnBookClick(item._id) }
                                                >
                                                    Вернуть книгу!
                                                </Button> :
                                                null
                                    }
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    )
}