import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import AuthStore, { AuthStateType } from '../Ducks/auth'
import UserStore, { UserStateType } from '../Ducks/user'
import BooksStore, { BooksStateType } from '../Ducks/books'
import { History } from 'history';

export type StoreType = {
    auth: AuthStateType,
    user: UserStateType
    books: BooksStateType
};
export default function(history: History) {
    return combineReducers({
        router: connectRouter(history),
        auth: AuthStore,
        user: UserStore,
        books: BooksStore,
    });
}
