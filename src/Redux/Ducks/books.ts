import { AddBookType, BookType } from '../../types/books';
import { StoreType } from '../Store';
import { returnFailState, returnStartState, returnSuccessState } from '../../utils';
import { UserType } from '../../types/user';

export const BooksConsts = {
    fetchBooksSaga: 'FETCH_BOOKS_SAGA' as const,
    fetchBooksStart: 'FETCH_BOOKS_START' as const,
    fetchBooksFail: 'FETCH_BOOKS_FAIL' as const,
    fetchBooksSuccess: 'FETCH_BOOKS_SUCCESS' as const,
    addBookSaga: 'ADD_BOOK_SAGA' as const,
    addBookStart: 'ADD_BOOK_START' as const,
    addBookFail: 'ADD_BOOK_FAIL' as const,
    addBookSuccess: 'ADD_BOOK_SUCCESS' as const,
    takeBookSaga: 'TAKE_BOOK_SAGA' as const,
    takeBookStart: 'TAKE_BOOK_START' as const,
    takeBookFail: 'TAKE_BOOK_FAIL' as const,
    takeBookSuccess: 'TAKE_BOOK_SUCCESS' as const,
    returnBookSaga: 'RETURN_BOOK_SAGA' as const,
    returnBookStart: 'RETURN_BOOK_START' as const,
    returnBookFail: 'RETURN_BOOK_FAIL' as const,
    returnBookSuccess: 'RETURN_BOOK_SUCCESS' as const,
};

export function fetchBooksSagaAC() {
    return { type: BooksConsts.fetchBooksSaga }
}

export function fetchBooksStartAC() {
    return { type: BooksConsts.fetchBooksStart }
}

export function fetchBooksFailAC() {
    return { type: BooksConsts.fetchBooksFail }
}

export function fetchBooksSuccessAC(books: [BookType]) {
    return { type: BooksConsts.fetchBooksSuccess, payload: books }
}

export function addBookSagaAC(book: AddBookType) {
    return { type: BooksConsts.addBookSaga, payload: book }
}

export function addBookStartAC() {
    return { type: BooksConsts.addBookStart }
}

export function addBookFailAC() {
    return { type: BooksConsts.addBookFail }
}

export function addBookSuccessAC(book: BookType) {
    return { type: BooksConsts.addBookSuccess, payload: book }
}

export function takeBookSagaAC(id: string) {
    return { type: BooksConsts.takeBookSaga, payload: id }
}

export function takeBookStartAC() {
    return { type: BooksConsts.takeBookStart }
}

export function takeBookFailAC() {
    return { type: BooksConsts.takeBookFail }
}

export function takeBookSuccessAC(book: BookType, user: UserType) {
    return { type: BooksConsts.takeBookSuccess, payload: { book, user } }
}


export function returnBookSagaAC(id: string) {
    return { type: BooksConsts.returnBookSaga, payload: id }
}

export function returnBookStartAC() {
    return { type: BooksConsts.returnBookStart }
}

export function returnBookFailAC() {
    return { type: BooksConsts.returnBookFail }
}

export function returnBookSuccessAC(book: BookType) {
    return { type: BooksConsts.returnBookSuccess, payload: { book } }
}

export type ActionCreatorsType = ReturnType<typeof fetchBooksSagaAC>
    | ReturnType<typeof fetchBooksStartAC>
    | ReturnType<typeof fetchBooksFailAC>
    | ReturnType<typeof fetchBooksSuccessAC>
    | ReturnType<typeof addBookSagaAC>
    | ReturnType<typeof addBookStartAC>
    | ReturnType<typeof addBookFailAC>
    | ReturnType<typeof addBookSuccessAC>
    | ReturnType<typeof takeBookSagaAC>
    | ReturnType<typeof takeBookStartAC>
    | ReturnType<typeof takeBookFailAC>
    | ReturnType<typeof takeBookSuccessAC>
    | ReturnType<typeof returnBookSagaAC>
    | ReturnType<typeof returnBookStartAC>
    | ReturnType<typeof returnBookFailAC>
    | ReturnType<typeof returnBookSuccessAC>

export type BooksStateType = {
    books: BookType[],
    isLoading: boolean,
    isError: boolean,
}

const initState: BooksStateType = {
    books: [],
    isLoading: false,
    isError: false,
};

export default function(state: BooksStateType = initState, action: ActionCreatorsType): BooksStateType {
    switch (action.type) {
        case BooksConsts.fetchBooksStart: {
            return {
                ...state,
                ...returnStartState()
            }
        }
        case BooksConsts.fetchBooksFail: {
            return {
                ...state,
                ...returnFailState()
            }
        }
        case BooksConsts.fetchBooksSuccess: {
            return {
                ...state,
                ...returnSuccessState(),
                books: action.payload,
            }
        }
        case BooksConsts.addBookSuccess: {
            return {
                ...state,
                ...returnStartState(),
                books: ([] as BookType[]).concat(state.books).concat([action.payload])
            }
        }
        case BooksConsts.takeBookSuccess: {
            let initBook = state.books.find(function (item) {
                return item._id === action.payload.book._id
            });
            if (!initBook) return state;
            initBook.reader = action.payload.user;
            return {
                ...state,
                ...returnSuccessState(),
                books: ([] as BookType[]).concat(state.books)
            }
        }
        case BooksConsts.returnBookSuccess: {
            let initBook = state.books.find(function (item) {
                return item._id === action.payload.book._id
            });
            if (!initBook) return state;
            initBook.reader = undefined;
            return {
                ...state,
                ...returnSuccessState(),
                books: ([] as BookType[]).concat(state.books)
            }
        }
        default: {
            return state
        }
    }
}

export function getBooks(state: StoreType): BooksStateType {
    return state.books;
}
