import {
    SignUpDataType,
} from '../../types/user';
import { StoreType } from '../Store';
import {
    initialProcessState, ProcessStateType,
    returnFailState, returnStartState, returnSuccessState
} from '../../utils';


export const AuthConsts = {
    signUpSaga: 'SIGN_UP_SAGA' as const,
    signUpStart: 'SIGN_UP_START' as const,
    signUpSuccess: 'SIGN_UP_SUCCESS' as const,
    signUpFail: 'SIGN_UP_FAIL' as const,
    loginSaga: 'LOGIN_SAGA' as const,
    loginStart: 'LOGIN_START' as const,
    loginSuccess: 'LOGIN_SUCCESS' as const,
    loginFail: 'LOGIN_FAIL' as const,
    logoutSaga: 'LOGOUT_SAGA' as const,
    logoutStart: 'LOGOUT_START' as const,
    logoutSuccess: 'LOGOUT_SUCCESS' as const,
    logoutFail: 'LOGOUT_FAIL' as const,
};

export function signUpSagaAC(payload: SignUpDataType) {
    return {
        type: AuthConsts.signUpSaga,
        payload,
    }
}
export function signUpStartAC() {
    return {
        type: AuthConsts.signUpStart,
    }
}
export function signUpSuccessAC() {
    return {
        type: AuthConsts.signUpSuccess,
    }
}
export function signUpFailAC() {
    return {
        type: AuthConsts.signUpFail,
    }
}

export function loginSagaAC(payload: SignUpDataType) {
    return {
        type: AuthConsts.loginSaga,
        payload,
    }
}
export function loginStartAC() {
    return {
        type: AuthConsts.loginStart,
    }
}
export function loginSuccessAC() {
    return {
        type: AuthConsts.loginSuccess,
    }
}
export function loginFailAC() {
    return {
        type: AuthConsts.loginFail,
    }
}

export function logoutSagaAC() {
    return {
        type: AuthConsts.logoutSaga,
    }
}
export function logoutStartAC() {
    return {
        type: AuthConsts.logoutStart,
    }
}
export function logoutSuccessAC() {
    return {
        type: AuthConsts.logoutSuccess,
    }
}
export function logoutFailAC() {
    return {
        type: AuthConsts.logoutFail,
    }
}

type ActionCreatorTypes = ReturnType<typeof signUpSagaAC>
    | ReturnType<typeof signUpStartAC>
    | ReturnType<typeof signUpSuccessAC>
    | ReturnType<typeof signUpFailAC>
    | ReturnType<typeof loginSagaAC>
    | ReturnType<typeof loginStartAC>
    | ReturnType<typeof loginSuccessAC>
    | ReturnType<typeof loginFailAC>
    | ReturnType<typeof logoutSagaAC>
    | ReturnType<typeof logoutStartAC>
    | ReturnType<typeof logoutSuccessAC>
    | ReturnType<typeof logoutFailAC>

export type AuthStateType = ProcessStateType;

const initialState: AuthStateType = initialProcessState;

export default function(state: AuthStateType = initialState, action:ActionCreatorTypes): AuthStateType {
    switch (action.type) {
        case AuthConsts.signUpStart:
        case AuthConsts.logoutStart:
        case AuthConsts.loginStart: {
            return {
                ...returnStartState()
            }
        }
        case AuthConsts.signUpSuccess:
        case AuthConsts.logoutSuccess:
        case AuthConsts.loginSuccess: {
            return {
                ...returnSuccessState()
            }
        }
        case AuthConsts.signUpFail:
        case AuthConsts.logoutFail:
        case AuthConsts.loginFail: {
            return {
                ...returnFailState()
            }
        }
        default: {
            return state;
        }
    }
}

export function getAuthState(state: StoreType) {
    return state.auth;
}