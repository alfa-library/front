import {
    defaultUser,
    UserType
} from '../../types/user';
import { StoreType } from '../Store';
import {
    initialProcessState, ProcessStateType,
    returnFailState, returnStartState, returnSuccessState
} from '../../utils';

export const UserConsts = {
    fetchUserSaga: 'FETCH_USER_SAGA' as const,
    fetchUserStart: 'FETCH_USER_START' as const,
    fetchUserFail: 'FETCH_USER_FAIL' as const,
    fetchUserSuccess: 'FETCH_USER_SUCCESS' as const,
};

export function fetchUserSagaAC () {
    return {
        type: UserConsts.fetchUserSaga
    }
}

export function fetchUserStartAC () {
    return {
        type: UserConsts.fetchUserStart
    }
}

export function fetchUserFailAC () {
    return {
        type: UserConsts.fetchUserFail
    }
}

export function fetchUserSuccessAC (user: UserType) {
    return {
        type: UserConsts.fetchUserSuccess,
        payload: user
    }
}

type ActionCreatorTypes = ReturnType<typeof fetchUserSagaAC>
    | ReturnType<typeof fetchUserStartAC>
    | ReturnType<typeof fetchUserFailAC>
    | ReturnType<typeof fetchUserSuccessAC>

export type UserStateType = {
    user: UserType,
    processing: ProcessStateType
};

const initialState: UserStateType = {
    user: defaultUser,
    processing: initialProcessState
};

export default function(state: UserStateType = initialState, action:ActionCreatorTypes): UserStateType {
    switch (action.type) {
        case UserConsts.fetchUserStart: {
            return {
                ...state,
                processing: returnStartState()
            }
        }

        case UserConsts.fetchUserFail: {
            return {
                ...state,
                processing: returnFailState()
            }
        }

        case UserConsts.fetchUserSuccess: {
            return {
                ...state,
                user: action.payload,
                processing: returnSuccessState()
            }
        }
        default: {
            return state;
        }
    }
}

export function getUser(state: StoreType):UserType {
    return state.user.user;
}

export function getUserProcessing(state: StoreType):ProcessStateType {
    return state.user.processing;
}