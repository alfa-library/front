import { call, put, takeEvery } from '@redux-saga/core/effects';
import { push } from 'connected-react-router'
import {
    AuthConsts,
    loginFailAC, loginSagaAC, loginStartAC, loginSuccessAC, logoutFailAC, logoutStartAC, logoutSuccessAC,
    signUpFailAC, signUpSagaAC, signUpStartAC, signUpSuccessAC
} from '../Ducks/auth';
import {
    fetchUserSuccessAC
} from '../Ducks/user';
import { signUpPostFetcher, loginPostFetcher, logoutPostFetcher } from '../../fetchers/auth';
import { defaultUser, UserType } from '../../types/user';

export function* watchSignUpSaga() {
    yield takeEvery(AuthConsts.signUpSaga, signUpSaga);
}

export function* watchLoginSaga() {
    yield takeEvery(AuthConsts.loginSaga, loginSaga);
}

export function* watchLogoutSaga() {
    yield takeEvery(AuthConsts.logoutSaga, logoutSaga);
}

export function* signUpSaga(action: ReturnType<typeof signUpSagaAC>) {
    yield put(signUpStartAC());
    try {
        yield call(signUpPostFetcher, action.payload);
        yield put(signUpSuccessAC())
    } catch(e) {
        yield put(signUpFailAC())
    }
}

export function* loginSaga(action: ReturnType<typeof loginSagaAC>) {
    yield put(loginStartAC());
    try {
        const res:{ data: UserType} = yield call(loginPostFetcher, action.payload);
        yield put(loginSuccessAC());
        yield put(fetchUserSuccessAC(res.data));
        yield put(push('/'));
    } catch(e) {
        yield put(loginFailAC())
    }
}

export function* logoutSaga() {
    yield put(logoutStartAC());
    try {
        yield call(logoutPostFetcher);
        yield put(logoutSuccessAC());
        yield put(fetchUserSuccessAC(defaultUser));
        yield put(push('/login'));
    } catch(e) {
        yield put(logoutFailAC())
    }
}
