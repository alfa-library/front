import { all } from 'redux-saga/effects';
import { watchLoginSaga, watchLogoutSaga, watchSignUpSaga } from './auth';
import { watchFetchUserSaga } from './fecthUser';
import { watchAddBookSaga, watchFetchBooksSaga, watchReturnBookSaga, watchTakeBookSaga } from './books';


export default function* rootSaga() {
    yield all([
        watchSignUpSaga(),
        watchLoginSaga(),
        watchLogoutSaga(),
        watchFetchUserSaga(),
        watchFetchBooksSaga(),
        watchAddBookSaga(),
        watchTakeBookSaga(),
        watchReturnBookSaga(),
    ]);
}
