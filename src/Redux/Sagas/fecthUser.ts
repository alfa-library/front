import { call, put, takeEvery } from '@redux-saga/core/effects';
import {
    fetchUserFailAC,
    fetchUserStartAC,
    UserConsts

} from '../Ducks/user';
import {
    fetchUserSuccessAC
} from '../Ducks/user';
import { UserType } from '../../types/user';
import { fetchUserFetcher } from '../../fetchers/user';
import { checkUnauthorizedError } from './utils';

export function* watchFetchUserSaga() {
    yield takeEvery(UserConsts.fetchUserSaga, fetchUserSaga);
}

export function* fetchUserSaga() {
    yield put(fetchUserStartAC());
    try {
        const res: { data: UserType } = yield call(fetchUserFetcher);
        yield put(fetchUserSuccessAC(res.data));
    } catch (e) {
        yield checkUnauthorizedError(e);
        yield put(fetchUserFailAC())
    }
}
