import { put } from '@redux-saga/core/effects';
import { push } from "connected-react-router";
import { AxiosError } from 'axios';

export function* checkUnauthorizedError(e: AxiosError) {
    if (e.response && e.response.status === 401) {
        yield put(push('/login'));
    }
}