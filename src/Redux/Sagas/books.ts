import { push } from 'connected-react-router';
import { call, put, takeEvery, select } from '@redux-saga/core/effects';
import {
    BooksConsts,
    addBookFailAC, addBookSagaAC, addBookStartAC, addBookSuccessAC,
    fetchBooksFailAC, fetchBooksStartAC, fetchBooksSuccessAC,
    takeBookSagaAC, takeBookStartAC, takeBookSuccessAC, takeBookFailAC,
    returnBookSagaAC, returnBookStartAC, returnBookSuccessAC, returnBookFailAC
} from '../Ducks/books';
import { addBookFetcher, fetchBooksFetcher, returnBookFetcher, takeBookFetcher } from '../../fetchers/books';
import { BookType } from '../../types/books';
import { UserType } from '../../types/user';
import { getUser } from '../Ducks/user';
import { checkUnauthorizedError } from './utils';

export function* watchFetchBooksSaga() {
    yield takeEvery(BooksConsts.fetchBooksSaga, fetchBooksSaga);
}

export function* watchAddBookSaga() {
    yield takeEvery(BooksConsts.addBookSaga, addBookSaga);
}

export function* watchTakeBookSaga() {
    yield takeEvery(BooksConsts.takeBookSaga, takeBookSaga);
}

export function* watchReturnBookSaga() {
    yield takeEvery(BooksConsts.returnBookSaga, returnBookSaga);
}

export function* fetchBooksSaga() {
    yield put(fetchBooksStartAC());
    try {
        const res: { data: [BookType]} = yield call(fetchBooksFetcher);
        yield put(fetchBooksSuccessAC(res.data));
    } catch (e) {
        yield put(fetchBooksFailAC())
    }
}

export function* addBookSaga(action: ReturnType<typeof addBookSagaAC>) {
    yield put(addBookStartAC());
    try {
        const res: { data: BookType} = yield call(addBookFetcher, action.payload);
        yield put(addBookSuccessAC(res.data));
        yield put(push('/'))
    } catch (e) {
        yield checkUnauthorizedError(e);
        yield put(addBookFailAC())
    }
}

export function* takeBookSaga(action: ReturnType<typeof takeBookSagaAC>) {
    yield put(takeBookStartAC());
    try {
        const res: { data: BookType} = yield call(takeBookFetcher, action.payload);
        const user: UserType = yield select(getUser);
        yield put(takeBookSuccessAC(res.data, user));
    } catch (e) {
        yield checkUnauthorizedError(e);
        yield put(takeBookFailAC())
    }
}

export function* returnBookSaga(action: ReturnType<typeof returnBookSagaAC>) {
    yield put(returnBookStartAC());
    try {
        const res: { data: BookType} = yield call(returnBookFetcher, action.payload);
        yield put(returnBookSuccessAC(res.data));
    } catch (e) {
        yield checkUnauthorizedError(e);
        yield put(returnBookFailAC())
    }
}