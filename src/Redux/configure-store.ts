import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory, History } from 'history'
import { routerMiddleware } from 'connected-react-router'
import createReducers from './Store/index';
import rootSaga from './Sagas/index';

const composeEnhancers = composeWithDevTools({});

export const history: History = createBrowserHistory();

export default function configureStore() {
    return (initState = {}) => {
        const sagaMiddleware = createSagaMiddleware();

        const store = createStore(
            createReducers(history),
            initState,
            composeEnhancers(applyMiddleware(
                sagaMiddleware,
                routerMiddleware(history)))
        );

        sagaMiddleware.run(rootSaga);

        return store;
    };
}
