const getConfig = require('arui-presets/postcss');
const mq = require('arui-feather/mq/mq.json');

module.exports = getConfig(mq);
